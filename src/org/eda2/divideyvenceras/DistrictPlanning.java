package org.eda2.divideyvenceras;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

public class DistrictPlanning {

	private int[] distritos;
	private ArrayList<Integer> listaAbsentismo;
	private int numeroManzanas;
	private TreeMap<Integer, Manzana> fractalToManhattan;

	public DistrictPlanning(int[] distritos) {
		 this.distritos = distritos;
		 listaAbsentismo = new ArrayList<Integer>();
		 numeroManzanas = potenciaDeCuatroContenida(distritos.length);
		 fractalToManhattan = new TreeMap<Integer, Manzana>();
	}

	public DistrictPlanning(int[][] manzanas) {
		numeroManzanas = manzanas.length * manzanas[0].length;
		int unaDimension = 0;
		int resto = manzanas.length * manzanas[0].length;
		while(resto > 0) {
			unaDimension += resto;
			resto = resto / 4;
		}
		distritos = new int[unaDimension];
		procesamientoDatos(manzanas);
		listaAbsentismo = new ArrayList<Integer>();
		fractalToManhattan = new TreeMap<Integer, Manzana>();
	}

	public DistrictPlanning(String archivo){
		fractalToManhattan = new TreeMap<Integer, Manzana>();
		listaAbsentismo = new ArrayList<Integer>();
		ArrayList<Integer> lista = new ArrayList<Integer>();
		int numero = 0;
		int sumaHijos = 0;
		try{
		File file = new File(archivo);
		BufferedReader br = new BufferedReader(new FileReader(file));
		String linea = "";
		while(br.ready()){
			linea = br.readLine();
			numero = Integer.parseInt(linea);
			if(numero%1==0)
				lista.add(numero);
		}
		br.close();
		
		this.numeroManzanas = potenciaDeCuatroContenida(lista.size());
		int numeroNoManzanas = lista.size() - numeroManzanas;
		if ( sumaPotenciasMenores(4, (int)logOfBase(4, numeroManzanas))!=lista.size()) {
			throw new RuntimeException("Hubo una perdida de datos");
		}
		
		for (int i = 0; i < numeroNoManzanas; i++) {
			sumaHijos = lista.get(i*4+1) + lista.get(i*4+2) + lista.get(i*4+3) + lista.get(i*4+4);
			if(sumaHijos != lista.get(i))
				throw new RuntimeException("Datos corruptos");
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
		finally {
			distritos = new int[lista.size()];
			for (int i = 0; i < distritos.length; i++) {
				distritos[i] = lista.get(i);
			}
		}
	}

	// Procesamiento de datos

	//llamado desde el constructor de planificaci�n del distrinto para rellenar la estructura de datos
	public void procesamientoDatos(int[][] matrizManzanas) {
		procesamientoManzanasDyV(matrizManzanas, matrizManzanas.length-1, 0, 0, matrizManzanas[0].length-1, 0);

	}

	private void procesamientoManzanasDyV(int[][] matrizManzanas, int ComienzoX, int ComienzoY, int FinX, int FinY, int current) {
		if (current >= distritos.length)return;
		
		int mediaX = (ComienzoX + FinX)/2;
		int mediaY = (ComienzoY + FinY)/2;
		
		int aux = 0;
		for (int i = ComienzoX; i >= FinX; i--) {
			for (int j = ComienzoY; j <= FinY; j++) {
				aux += matrizManzanas[i][j];
			}
		}
		distritos[current] = aux;

		if(4*current+2 > distritos.length) return;
		procesamientoManzanasDyV(matrizManzanas, ComienzoX, ComienzoY, mediaX+1, mediaY, 4*current+1);
		procesamientoManzanasDyV(matrizManzanas, mediaX, ComienzoY, FinX, mediaY, 4*current+2);
		procesamientoManzanasDyV( matrizManzanas, mediaX, mediaY+1, FinX, FinY, 4*current+3);
		procesamientoManzanasDyV(matrizManzanas, ComienzoX, mediaY+1, mediaX+1, FinY, 4*current+4);
	}

	public ArrayList<Integer> getListaAbsentismo() {
		return listaAbsentismo;
	}

	public int[] getDistritos() {
		return distritos;
	}

	// Divide y venceras

	// Devolvera una lista con las manzanas con absentismo
	// la matriz de manzanas es temporal, en cuanto este preparado el procesamiento de datos, se usara.

	public ArrayList<Integer> districtPlanningDivideVenceras() {
		int current = 0;
		districtPlanningDivideVenceras(distritos, current);
		return listaAbsentismo;
	}

	private void districtPlanningDivideVenceras(int[] datos, int current) {
		if(datos[current] < 15) return;
		if((datos.length-numeroManzanas)<current+1) {
			listaAbsentismo.add(current);
		}
		if (current*4+1 >= datos.length)return;
		if (datos[current*4+1]>15) {
			districtPlanningDivideVenceras(datos, current*4+1);
		}
		if (datos[current*4+2]>15) {
			districtPlanningDivideVenceras(datos, current*4+2);
		}
		if (datos[current*4+3]>15) {
			districtPlanningDivideVenceras(datos, current*4+3);
		}
		if (datos[current*4+4]>15) {
			districtPlanningDivideVenceras(datos, current*4+4);
		}
	}

	private int potenciaDeCuatroContenida(int numero) {
		int aux = 1;
		while(aux < numero) {
			aux*=4;
		}
		return aux/4;
	}
	
	private int sumaPotenciasMenores(int x, int e) { 
		int aux = 0;
		for (int i = 0; i <= e; i++) {
			aux += Math.pow(x, i);
		}
		return aux;
	}
	
	public double logOfBase(int base, int num) {
	    return Math.log(num) / Math.log(base);
	}

	// Post procesamiento - Transformaci�n de posiciones

	// Duevuelve una tabla que contiene la traducción de distritos a formato de manhattan
	public TreeMap<Integer, Manzana> toManhattan(ArrayList<Integer> lista) {
		
		int aux = (int) Math.sqrt(potenciaDeCuatroContenida(distritos.length));

		toManhattan(lista, aux-1, 0, 0, aux-1, 0);

		return fractalToManhattan;
	}

	//metodo fractal
	private void toManhattan(ArrayList<Integer> absentismo, int inicioX, int inicioY, int finX, int finY, int current) {
		if (current >= distritos.length)return;
		int mediaX = (inicioX + finX)/2;
		int mediaY = (inicioY + finY)/2;
		Manzana manzana = new Manzana(inicioX, inicioY);
		if(absentismo.contains(current)) {
			fractalToManhattan.put(current, manzana);
		}

		if(4*current+1 > distritos.length) return;
		toManhattan(absentismo, inicioX, inicioY, mediaX+1, mediaY, 4*current+1);
		toManhattan(absentismo, mediaX, inicioY, finX, mediaY, 4*current+2);
		toManhattan(absentismo, mediaX, mediaY+1, finX, finY, 4*current+3);
		toManhattan(absentismo, inicioX, mediaY+1, mediaX+1, finY, 4*current+4);
	}
}
