package org.eda2.divideyvenceras;

public class Manzana {

	private int x;
	private int y;
	
	public Manzana(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public String toString() {
		return "["+x+", "+y+"]";
	}
}
