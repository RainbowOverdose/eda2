package org.eda2.divideyvenceras;

public class MedidasRendimiento {

	public static void main(String[] args) {
		int citySize = 2;
		double[] tiempos = new double[10];

		System.out.println("Mejor caso:");
		for (int k = 0; k < 13; k++) {
			int[][] matriz = new int[citySize][citySize];

			for (int l = 0; l < 10; l++) {
				for (int i = 0; i < matriz.length; i++) {
					for (int j = 0; j < matriz[i].length; j++) {
						matriz[i][j] = (int)(Math.random()*4);
					}
				}

				DistrictPlanning dp = new DistrictPlanning(matriz);
				tiempos[l] = System.nanoTime()*Math.pow(10, -9);
				dp.districtPlanningDivideVenceras();
				tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
			}
			System.out.printf("%f s ",media(tiempos));
			citySize *= 2;
		}

		System.out.println("\nPeor caso:");
		citySize = 2;

		for (int k = 0; k < 13; k++) {
			int[][] matriz = new int[citySize][citySize];

			for (int l = 0; l < 10; l++) {
				for (int i = 0; i < matriz.length; i++) {
					for (int j = 0; j < matriz[i].length; j++) {
						matriz[i][j] = (int)(Math.random()*30+14);
					}
				}

				DistrictPlanning dp = new DistrictPlanning(matriz);
				tiempos[l] = System.nanoTime()*Math.pow(10, -9);
				dp.districtPlanningDivideVenceras();
				tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
			}
			System.out.printf("%f s ",media(tiempos));
			citySize *= 2;
		}

		System.out.println("\nCaso promedio:");
		citySize = 2;

		for (int k = 0; k < 13; k++) {
			int[][] matriz = new int[citySize][citySize];

			for (int l = 0; l < 10; l++) {
				for (int i = 0; i < matriz.length; i++) {
					for (int j = 0; j < matriz[i].length; j++) {
						matriz[i][j] = (int)(Math.random()*26);
					}
				}

				DistrictPlanning dp = new DistrictPlanning(matriz);
				tiempos[l] = System.nanoTime()*Math.pow(10, -9);
				dp.districtPlanningDivideVenceras();
				tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
			}
			System.out.printf("%f s ",media(tiempos));
			citySize *= 2;
		}

	}

	public static double media (double[] array) {
		double media = 0;
		for (int i = 0; i < array.length; i++) {
			media += array[i];
		}
		media /= array.length;
		return media;
	}
}
