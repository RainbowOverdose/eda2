package org.eda2.divideyvenceras;

import java.io.File;
import java.util.ArrayList;
import java.util.TreeMap;

public class PlanningOffice {

	public static void main(String[] args) {
		String archivoCiudad = System.getProperty("user.dir") + File.separator +
			    "src" + File.separator +
			    "org" + File.separator +
			    "eda2" + File.separator +
			    "divideyvenceras" + File.separator +
			    "ciudad16x16.txt";
		int[] array = {148, 34, 34, 48, 34 , 4, 17, 2, 11, 4, 17, 2, 11, 16, 17, 2, 11, 4, 17, 2, 11};
		DistrictPlanning dp = new DistrictPlanning(array);
		dp.districtPlanningDivideVenceras();
		ArrayList<Integer> listaAbsentismo = dp.getListaAbsentismo();
		for (int i = 0; i < listaAbsentismo.size(); i++) {

			System.out.print(listaAbsentismo.get(i).toString());
			System.out.println(" : " +array[listaAbsentismo.get(i)]);
		}

		System.out.println("");
		System.out.println("**********A partir de una matriz hecha a mano: **********");
		System.out.println("");

		int[][] matriz = {{4, 17, 2, 11, 4, 17, 2, 11},
							{4, 17, 2, 11, 4, 17, 2, 11},
							{4, 17, 2, 11, 4, 17, 2, 11},
							{4, 17, 2, 11, 4, 17, 2, 11},
							{4, 17, 2, 11, 4, 1052, 2, 11},
							{4, 17, 2, 11, 4, 17, 2, 11},
							{4, 28, 2, 11, 4, 17, 2, 11},
							{4, 17, 2, 11, 4, 17, 2, 11}};
		DistrictPlanning dp2 = new DistrictPlanning(matriz);
		dp2.districtPlanningDivideVenceras();
		ArrayList<Integer> listaAbsentismo2 = dp2.getListaAbsentismo();
		int[] array2 = dp2.getDistritos();
		//for (int i = 0; i < listaAbsentismo2.size(); i++) {

		//	System.out.print(listaAbsentismo2.get(i).toString());
		//	System.out.println(" : " +array2[listaAbsentismo2.get(i)]);

		//}

		System.out.println();
		//System.out.println("********* Valores pasados a Manhattan: **********");
		//System.out.println();

		TreeMap<Integer, Manzana> mapaManhattanAbsentismo = dp2.toManhattan(listaAbsentismo2);
		for (int i : mapaManhattanAbsentismo.keySet()) {
			System.out.println(i + ": "+array2[i] + " --> " + mapaManhattanAbsentismo.get(i));

		}

		int[][] matriz2 = new int[1024][1024];

		for (int i = 0; i < matriz2.length; i++) {
			for (int j = 0; j < matriz2[i].length; j++) {
				matriz2[i][j] = (int)(Math.random()*31);
			}
		}
		DistrictPlanning dp3 = new DistrictPlanning(matriz2);
		dp3.districtPlanningDivideVenceras();

		DistrictPlanning dp4 = new DistrictPlanning(archivoCiudad);
		dp4.districtPlanningDivideVenceras();
	}

}
