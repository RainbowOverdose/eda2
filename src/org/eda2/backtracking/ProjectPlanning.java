package org.eda2.backtracking;

import java.util.ArrayList;

public class ProjectPlanning {

	private int capacidad;
	private ArrayList<Integer> pesos;
	private ArrayList<Double> beneficios;
	private ArrayList<Integer> Sol;
	private ArrayList<Integer> parSol;
	private double mcd, presupuesto, maxBeneficio;


	public ProjectPlanning(ArrayList<Double> pesos, ArrayList<Double> beneficios, double presupuesto) {
		//constructor basico de pruebas
		this.presupuesto = presupuesto;
		this.maxBeneficio = 0;
		mcd = maximoComunDivisor(pesos);
		if(mcd>0)
			this.capacidad = (int)(presupuesto/mcd);
		else
			this.capacidad = 0;
		this.pesos = toPesosInt(pesos);
		this.beneficios = beneficios;
		Sol = new ArrayList<Integer>();
		parSol = new ArrayList<>();
		for (Integer i : this.pesos) {
			Sol.add(0);
			parSol.add(0);
		}
	}

	public ProjectPlanning(ArrayList<GrupoTrabajo> gruposTrabajo, ArrayList<Propuesta> propuestas, double presupuesto, boolean hola) {
		this.presupuesto = presupuesto;
		this.maxBeneficio = 0;
		mcd = maximoComunDivisorP(propuestas);
		if(mcd>0)
			this.capacidad = (int)(presupuesto/mcd);
		else
			this.capacidad = 0;
		this.pesos = new ArrayList<>();
		this.beneficios = new ArrayList<>();
		//Pretratamiento para calcular los pesos de cada objeto y su correspondiente beneficio
		pret(propuestas, gruposTrabajo);
		Sol = new ArrayList<Integer>();
		parSol = new ArrayList<>();
		for (Integer i : this.pesos) {
			Sol.add(0);
			parSol.add(0);
		}
		System.out.println("Pesos: "+this.pesos.toString());
		System.out.println("Beneficios: "+this.beneficios.toString());
	}

	//M�ximo com�n divisor seg�n un array donde ya tengamos los costes
	private double maximoComunDivisor(ArrayList<Double> costes) {
		double tmp, mult;
		int decimal=0;
		if(costes.size()>1){
			for(int i = 0;i<costes.size();i++){
				if((costes.get(i)+"").split("\\.")[1].length()>decimal)
					decimal=(costes.get(i)+"").split("\\.")[1].length();
			}
			mult = Math.pow(10, decimal);
			tmp = mcd((int)(costes.get(0)*mult),(int)(costes.get(1)*mult))/mult;
			for(int i=2;i<costes.size();i++){
				tmp = mcd((int)(tmp*mult),(int)(costes.get(i)*mult))/mult;
			}
			return tmp;
		}
		else if(costes.size()==1){
			for(int i = 0;i<costes.size();i++){
				if((costes.get(i)+"").split("\\.")[1].length()>decimal)
					decimal=(costes.get(i)+"").split("\\.")[1].length();
			}
			mult = Math.pow(10, decimal);
			tmp = mcd((int)(costes.get(0)*mult),(int)(presupuesto*mult))/mult;
			return tmp;
		}
		else
			return 0;
	}
	//M�ximo com�n multiplo de los costes de todas las propuestas
	private double maximoComunDivisorP(ArrayList<Propuesta> prop) {
		double tmp, mult;
		int decimal=0;
		if(prop.size()>1){
			for(int i = 0;i<prop.size();i++){
				if((prop.get(i).getCostePropuesta()+"").split("\\.")[1].length()>decimal)
					decimal=(prop.get(i).getCostePropuesta()+"").split("\\.")[1].length();
			}
			mult = Math.pow(10, decimal);
			tmp = mcd((int)(prop.get(0).getCostePropuesta()*mult),(int)(prop.get(1).getCostePropuesta()*mult))/mult;
			for(int i=2;i<prop.size();i++){
				tmp = mcd((int)(tmp*mult),(int)(prop.get(i).getCostePropuesta()*mult))/mult;
			}
			return tmp;
		}
		else if(prop.size()==1){
			for(int i = 0;i<prop.size();i++){
				if((prop.get(i).getCostePropuesta()+"").split("\\.")[1].length()>decimal)
					decimal=(prop.get(i).getCostePropuesta()+"").split("\\.")[1].length();
			}
			mult = Math.pow(10, decimal);
			tmp = mcd((int)(prop.get(0).getCostePropuesta()*mult),(int)(presupuesto*mult))/mult;
			return tmp;
		}
		else
			return 0;
	}
	//Algoritmo de Euclides
	private int mcd(int a, int b){
		int r;
		while(b!=0){
			r = a%b;
			a = b;
			b = r;
		}
		return a;
	}

	public ArrayList<Integer> ProyectosMochilaDinamica() {
		backtrackingKnapsack(0, this.capacidad, maxBeneficio);
		System.out.println("Soluci�n Antes: "+Sol);
		int pesoActual=0;
		if(Sol.size()!=0){
			int min=Integer.MAX_VALUE;
			for(int i=0;i<Sol.size();i++){
				if(Sol.get(i)==1){
					pesoActual+=pesos.get(i);
					if(pesos.get(i)<min)
						min=pesos.get(i);
				}
			}
			if(this.capacidad-pesoActual>0){
			ArrayList<Integer> pesosMenores=getMinPesos(min);
				if(pesosMenores.size()>0){
					int maxBen=-1;
					for(int i=0;i<pesosMenores.size();i++){
						if(beneficios.get(pesosMenores.get(i))>maxBen)
							maxBen=pesosMenores.get(i);
					}
					if(maxBen!=-1)
						Sol.set(maxBen, 1);
				}
			}
		}
		return Sol;
	}
	//Castear un array de n�meros reales a enteros
	public ArrayList<Integer> toPesosInt(ArrayList<Double> pes){
		ArrayList<Integer> pesInt = new ArrayList<>();
		for (Double d : pes) {
			pesInt.add((int) (d/mcd));
		}
		return pesInt;
	}
	//Obtiene un ArrayList con todos los pesos menores que @param="min" y que no contenga la soluci�n.
	public ArrayList<Integer> getMinPesos(int min){

		ArrayList<Integer> pesosMenores = new ArrayList<>();
		for(int i=0;i<this.pesos.size();i++){
			if(pesos.get(i)<min && Sol.get(i)!=1)
				pesosMenores.add(i);
		}
		return pesosMenores;
	}
	//Pretratamiento del ArrayList propuestas y grupos de trabajos y rellenar pesos y beneficios
	public void pret(ArrayList<Propuesta> propuestas, ArrayList<GrupoTrabajo> gruposTrabajo){
		int st=0;
		for(GrupoTrabajo g: gruposTrabajo){
			st+=g.getSuperficieCubierta();
		}
		for (Propuesta p : propuestas) {
			GrupoTrabajo group = gruposTrabajo.get(p.getGrupoTrabajo()-1);
			/*System.out.println("Coste Propuesta:"+p.getCostePropuesta());
			System.out.println("Grupo de Trabajo:"+p.getGrupoTrabajo());
			System.out.println("Indicador de Impacto:"+p.getIndicadorImpacto());
			System.out.println("N�mero de propuesta:"+p.getNumeroPropuesta());
			System.out.println("Orden de prioridad:"+p.getOrdenPrioridad());*/
			this.pesos.add((int)(p.getCostePropuesta()/mcd));
			double sup = (double)group.getSuperficieCubierta()/(double)st;
			double sup2 = sup*(1/(double)p.getOrdenPrioridad());
			double sup3 = (double)p.getIndicadorImpacto()*(double)group.getAbsentismoTotal()*sup2;
			//System.out.println("sup "+group.getSuperficieCubierta()+"st "+st);
			/*System.out.println("1 "+sup);
			System.out.println("2 "+sup2);
			System.out.println("3 "+sup3);*/
			this.beneficios.add(sup3);
		}
	}

	private void backtrackingKnapsack(int startIndex, int capacity, double maxValue){

		//Si el beneficio obtenido con la nueva soluci�n es mayor o igual que la que obtuvimos de la
		//anterior soluci�n, actualizamos la soluci�n final.
		if (maxValue >= this.maxBeneficio) {
			this.maxBeneficio = maxValue;
			this.Sol = (ArrayList<Integer>) parSol.clone();
		}
		//Si no hay m�s elementos a explorar (nodos hoja), sube de nivel.
		if (startIndex >= pesos.size()) {
			return;
		}
		//Si el objeto a a�adir cabe en la mochila, lo a�adimos y procederemos a restar este peso
		//al peso total, y a�adirle el beneficio de ese objeto. Actualizamos la soluci�n parcial
		//y seguimos explorando el �rbol.
		//Cuando ha vuelto de un nivel inferior, simplemente actualizamos la soluci�n parcial y eliminaremos
		//el �ltimo objeto introducido en la mochila, y procederiamos a buscar una soluci�n sin ese �ltimo objeto.
		//As� hasta llegar a la soluci�n �ptima.
		if (this.pesos.get(startIndex) <= capacity) {

			capacity -= pesos.get(startIndex);
			maxValue += beneficios.get(startIndex);

			parSol.set(startIndex, 1);

			backtrackingKnapsack(startIndex + 1, capacity, maxValue);

			parSol.set(startIndex, 0);
			capacity += pesos.get(startIndex);
			maxValue -= beneficios.get(startIndex);

		}
		parSol.set(startIndex, 0);
		backtrackingKnapsack(startIndex + 1, capacity, maxValue);
	}
}
