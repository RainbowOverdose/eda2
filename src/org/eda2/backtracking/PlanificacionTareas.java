package org.eda2.backtracking;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class PlanificacionTareas {

	private ArrayList<Requisitos> tareas;
	private ArrayList<Acciones> acciones;
	private ArrayList<GrupoTrabajo> grupos;
	private ArrayList<HashMap<String, Integer>> recursos;
	private int totalAbsentismo;
	private int minPerdidas;
	private int temporalPerdidas;
	private ArrayList<Acciones> temporalAcciones;
	private ArrayList<ArrayList<Integer>> realizacionGrupos;

	/*public PlanificacionTareas(ArrayList<Integer> sol, ArrayList<Requisitos> tareas, HashMap<String, Integer> recursos,
			ArrayList<GrupoTrabajo> gt) {
		this.Sol = sol;
		this.tareas = tareas;
		this.recursos = new ArrayList<>();
		this.grupos = gt;
		this.parSol = new ArrayList<>();
		this.acciones = new ArrayList<>();
		int sum=0;
		for (Requisitos r : tareas) {
			sum+=r.getDuracion();
		}
		tareas.sort(new Comparator<Requisitos>() {

			@Override
			public int compare(Requisitos o1, Requisitos o2) {
				return -(gt.get(o1.getGrupo()-1).getAbsentismoTotal()*o1.getDuracion() - gt.get(o2.getGrupo()-1).getAbsentismoTotal()*o2.getDuracion());
			}
		});
		// no le quitamos la duración del proyecto mas corto, ya que se modificara luego
		// al moverse por el arbol de exploración
		for(int i=0;i<sum-1;i++){
			this.recursos.add((HashMap<String, Integer>) recursos.clone());
		}
	}*/
	public PlanificacionTareas(ArrayList<Requisitos> tareas, HashMap<String, Integer> recursos, ArrayList<GrupoTrabajo> gt) {
		for (GrupoTrabajo grupoTrabajo : gt) {
			totalAbsentismo += grupoTrabajo.getAbsentismoTotal();
		}
		this.minPerdidas = Integer.MAX_VALUE;
		this.tareas = tareas;
		this.recursos = new ArrayList<>();
		this.grupos = gt;
		this.acciones = new ArrayList<>();
		this.realizacionGrupos = new ArrayList<>();
		this.temporalAcciones = new ArrayList<>();
		int sum=0;
		for (Requisitos r : tareas) {
			sum+=r.getDuracion();
		}
		tareas.sort(new Comparator<Requisitos>() {

			@Override
			public int compare(Requisitos o1, Requisitos o2) {
				return -(gt.get(o1.getGrupo()-1).getAbsentismoTotal()*o1.getDuracion() - gt.get(o2.getGrupo()-1).getAbsentismoTotal()*o2.getDuracion());
			}
		});
		// no le quitamos la duración del proyecto mas corto, ya que se modificara luego
		// al moverse por el arbol de exploración
		for(int i=0;i<sum;i++){
			this.recursos.add(new HashMap<String, Integer>(recursos));
			this.realizacionGrupos.add(new ArrayList<>());
		}
	}
	public void backtracking(){
		backtrackingRecursivo(0, 0);
		for (Acciones a : acciones) {
			System.out.println("Numero de Accion: "+a.getNumAccion());
			System.out.println("Mes de Comienzo: "+a.getMesComienzo());
			System.out.println("Mes de Finalizacion: "+a.getMesFinal());
			System.out.println("Grupo Perteneciente: "+a.getGrupo());
			System.out.println("Propuesta Perteneciente: "+a.getRecur().getPropuesta());
			System.out.println();
		}
	}

	private void backtrackingRecursivo(int tarea, int mes) {
		while((mes+tareas.get(tarea).getDuracion()) < recursos.size()){
			//System.out.println("recursos "+recursos.size());
			//System.out.println(mes+tareas.get(tarea).getDuracion());
			boolean valido = true;
			for(int k=0; k<tareas.get(tarea).getDuracion(); k++){
				if(!aceptable(tarea, mes+k))valido = false;
			}
			if (valido) {
				temporalAcciones.add(new Acciones(tarea+1, tareas.get(tarea).getGrupo(), mes+1, mes+tareas.get(tarea).getDuracion(), tareas.get(tarea)));
				delRecursos(tarea, mes);
				//nodo hoja
				if(tarea == tareas.size()-1) {
					//funcion que busca minimizar:
					temporalPerdidas = 0;
					for (ArrayList<Integer> cadaMes : realizacionGrupos) {
						for (Integer integer : cadaMes) {
							temporalPerdidas +=totalAbsentismo-grupos.get(tareas.get(tarea).getGrupo()-1).getAbsentismoTotal();
						}
					}
					//comprobamos si existe mejora en lasolucion actual
					if (temporalPerdidas < minPerdidas) {
						minPerdidas = temporalPerdidas;
						acciones = new ArrayList<>(temporalAcciones);
					}
					//devolvemos los recursos antes de retroceder para el nodo hoja
					addRecursos(tarea, mes);
					return;
				}else{
					backtrackingRecursivo(tarea+1, 0);
				}
				//devolvemos los recursos antes explorar cada hermano nuevo para cada nodo valido
				addRecursos(tarea, mes);
			}
			//pasamos al siguiente nodo hermano
			mes++;
		}
		return;
	}
	//Devuelve los recursos de la tarea indicada en sus correspondientes meses
	private void addRecursos(int tarea, int mes){
		//System.out.println(realizacionGrupos);
		for(int i=mes;i<mes+tareas.get(tarea).getDuracion();i++){
			//System.out.println(realizacionGrupos.get(i).toString());
			//System.out.println(realizacionGrupos.get(i).indexOf(tareas.get(tarea).getGrupo()));
			//System.out.println(i);
			//System.out.println("Grupo: "+tareas.get(tarea).getGrupo());
			realizacionGrupos.get(i).remove(realizacionGrupos.get(i).indexOf(tareas.get(tarea).getGrupo()));
			recursos.get(mes).put("trabajo", recursos.get(mes).get("trabajo")+tareas.get(tarea).getNecesidadTrabajador());
			recursos.get(mes).put("psicologo", recursos.get(mes).get("psicologo")+tareas.get(tarea).getNecesidadPsicologos());
			recursos.get(mes).put("transporte", recursos.get(mes).get("transporte")+tareas.get(tarea).getNecesidadTransporte());
			recursos.get(mes).put("doe", recursos.get(mes).get("doe")+tareas.get(tarea).getDOE());
		}
		if (!temporalAcciones.isEmpty()) {
			temporalAcciones.remove(temporalAcciones.size()-1);
		}
	}
	//Decrementa los recursos en los meses de la tarea indicada
	private void delRecursos(int tarea, int mes){
		for(int i=mes;i<mes+tareas.get(tarea).getDuracion();i++){
			realizacionGrupos.get(i).add(tareas.get(tarea).getGrupo());
			recursos.get(mes).put("trabajo", recursos.get(mes).get("trabajo")-tareas.get(tarea).getNecesidadTrabajador());
			recursos.get(mes).put("psicologo", recursos.get(mes).get("psicologo")-tareas.get(tarea).getNecesidadPsicologos());
			recursos.get(mes).put("transporte", recursos.get(mes).get("transporte")-tareas.get(tarea).getNecesidadTransporte());
			recursos.get(mes).put("doe", recursos.get(mes).get("doe")-tareas.get(tarea).getDOE());
		}
	}

	//Comprueba que la tarea pueda realizarse en los meses indicados
	private boolean aceptable(int tarea, int mes){
		if (recursos.get(mes).get("trabajo")-tareas.get(tarea).getNecesidadTrabajador()>=0
				&& recursos.get(mes).get("psicologo")-tareas.get(tarea).getNecesidadPsicologos()>=0
				&& recursos.get(mes).get("transporte")-tareas.get(tarea).getNecesidadTransporte()>=0
				&& recursos.get(mes).get("doe")-tareas.get(tarea).getDOE()>=0
				&& !realizacionGrupos.get(mes).contains(tareas.get(tarea).getGrupo())) {
			return true;
		}
		return false;
	}
}
