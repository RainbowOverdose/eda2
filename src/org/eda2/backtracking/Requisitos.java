package org.eda2.backtracking;

public class Requisitos {

	private int propuesta;
	private int grupo;
	private int duracion;
	private int necesidadTrabajador;
	private int necesidadTransporte;
	private int necesidadDOE;
	private int necesidadPsicologos;

	public Requisitos(int prop, int group, int dur, int nTrab, int nTran, int doe, int nPsi){
		this.propuesta = prop;
		this.grupo = group;
		this.duracion = dur;
		this.necesidadTrabajador = nTrab;
		this.necesidadTransporte = nTran;
		this.necesidadDOE = doe;
		this.necesidadPsicologos = nPsi;
	}
	public int getPropuesta() {
		return propuesta;
	}

	public int getGrupo() {
		return grupo;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public int getNecesidadTrabajador() {
		return necesidadTrabajador;
	}

	public int getNecesidadTransporte() {
		return necesidadTransporte;
	}

	public void setNecesidadTransporte(int necesidadTransporte) {
		this.necesidadTransporte = necesidadTransporte;
	}

	public int getDOE() {
		return necesidadDOE;
	}

	public int getNecesidadPsicologos() {
		return necesidadPsicologos;
	}
}
