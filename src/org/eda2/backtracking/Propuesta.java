package org.eda2.backtracking;

public class Propuesta {

	private int grupoTrabajo;
	private int numeroPropuesta;
	private double costePropuesta;
	private int ordenPrioridad;
	private int indicadorImpacto;

	public Propuesta(int grupo, int numPropuesta, double coste, int orden, int impacto) {
		this.grupoTrabajo = grupo;
		this.numeroPropuesta = numPropuesta;
		this.costePropuesta = coste;
		this.ordenPrioridad = orden;
		this.indicadorImpacto = impacto;
	}

	public double getCostePropuesta() {
		return costePropuesta;
	}

	public int getGrupoTrabajo() {
		return grupoTrabajo;
	}

	public int getIndicadorImpacto() {
		return indicadorImpacto;
	}

	public int getNumeroPropuesta() {
		return numeroPropuesta;
	}

	public int getOrdenPrioridad() {
		return ordenPrioridad;
	}

}
