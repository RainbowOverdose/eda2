package org.eda2.backtracking;

import java.util.ArrayList;
import java.util.HashMap;

public class MedidasRendimiento {

	public static void main(String[] args) {
		//aqui se medira el tiempo de ejecucion para distintos tama�os n
		// y distinta complejidad
		int numPropuestas = 1;
		double[] tiempos = new double[10];
		//int count = 0;
		int aux = 0;

		//mejor caso que el mcd sea muy alto, entonces reduce la tabla mucho mas (tabla de n * m)este sera el m
		//el n se reduce si cogemos menos objetos con mas peso.
		/*System.out.println("MEJOR CASO");
		for (int k = 0; k < 10; k++) {
			int[][] matriz = new int[citySize][citySize];

			for (int l = 0; l < 10; l++) {
				count = 0;
				aux = citySize/2;
				for (int i = 0; i < matriz.length; i++) {
					for (int j = 0; j < matriz[i].length; j++) {
						matriz[i][j] = (int)(Math.random()*17);
					}
				}
				matriz[0][0] = 25;//nos aseguramos que exista al menos una manzana que supere el absentismo


				DistrictPlanning dp = new DistrictPlanning(matriz);
				dp.districtPlanningGreedy();
				ArrayList<GrupoTrabajo> gt = dp.workGroupsGreedy();
				ArrayList<Propuesta> p = new ArrayList<Propuesta>();
				int group = 0;
				int orden = 0;
				for(int i = 0; i < gt.size()*3; i++){
					orden= 1;
					group = (int)(Math.random()*gt.size()+1);
					for(int j = 0; j < p.size(); j++ ){
						if (p.get(j).getGrupoTrabajo() == group ) {
							orden++;
						}
					}

					p.add(new Propuesta(group, i, (int)(Math.random()*7+1)*5, orden, (int)Math.random()*4+1));
				}
				ProjectPlanning pp = new ProjectPlanning(gt, p, gt.size()*20, true);
				tiempos[l] = System.nanoTime()*Math.pow(10, -9);
				pp.ProyectosMochilaDinamica();
				tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
			}
			System.out.printf("%f s ",media(tiempos));
			citySize *= 2;
		}*/
		/*
		System.out.println("PEOR CASO");
		//El peor caso es que el mcd sea muy bajo, entonces aumenta la tabla mucho mas (tabla de n * m)este sera el m
		//el n aumenta si cogemos mayor numero de objetos con menor peso.
		for (int k = 0; k < 10; k++) {
			int[][] matriz = new int[citySize][citySize];

			for (int l = 0; l < 10; l++) {
				count = 0;
				aux = citySize/2;
				for (int i = 0; i < matriz.length; i++) {
					for (int j = 0; j < matriz[i].length; j++) {
						matriz[i][j] = (int)(Math.random()*30);
					}
				}
				matriz[0][0] = 25;//nos aseguramos que exista al menos una manzana que supere el absentismo


				DistrictPlanning dp = new DistrictPlanning(matriz);
				dp.districtPlanningGreedy();
				ArrayList<GrupoTrabajo> gt = dp.workGroupsGreedy();
				ArrayList<Propuesta> p = new ArrayList<Propuesta>();
				int group = 0;
				int orden = 0;
				for( int i = 0; i < gt.size()*3; i++){
					orden= 1;
					group = (int)(Math.random()*gt.size()+1);
					for(int j = 0; j < p.size(); j++ ){
						if (p.get(j).getGrupoTrabajo() == group ) {
							orden++;
						}
					}

					p.add(new Propuesta(group, i, (int)(Math.random()*35+1), orden, (int)Math.random()*4+1));
				}
				ProjectPlanning pp = new ProjectPlanning(gt, p, gt.size()*20, true);
				tiempos[l] = System.nanoTime()*Math.pow(10, -9);
				pp.ProyectosMochilaDinamica();
				tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
			}
			System.out.printf("%f s ",media(tiempos));
			citySize *= 2;

		}*/
		//Generamos los grupos de trabajo
		int doe = 0;
		double posibilidadDoe = 0.5;
		int[][] matriz3 = {{5,20,1,0,5,3,2,1},
				   {12,9,2,7,4,7,2,4},
				   {1,0,8,7,5,3,2,3},
				   {2,7,10,1,2,1,7,6},
				   {6,7,4,1,83,13,7,8},
				   {12,1,3,5,6,3,1,8},
				   {50,4,3,2,1,7,6,8},
				   {67,3,4,2,3,6,8,9}};

		DistrictPlanning dp5 = new DistrictPlanning(matriz3);
		dp5.districtPlanningGreedy();
		ArrayList<GrupoTrabajo> gt = dp5.workGroupsGreedy();
		ArrayList<Requisitos> tareas;
		PlanificacionTareas pt;
		
		HashMap<String, Integer> recursos = new HashMap<>();
		recursos.put("trabajo", 9);
		recursos.put("psicologo", 9);
		recursos.put("transporte", 9);
		recursos.put("doe", 1);
		
		for (int k = 0; k < 16; k++) {
			for (int i = 0; i < 10; i++) {
				tareas = new ArrayList<Requisitos>();
				for (int j = 0; j < numPropuestas; j++) {
					aux = (int)Math.random()*4+1;
					doe = Math.random() < posibilidadDoe?1:0;
					tareas.add(new Requisitos(j, aux, 1, (int)Math.random()*3+1, (int)Math.random()*3+1, doe, (int)Math.random()*3+1));
				}
				pt = new PlanificacionTareas(tareas, recursos, gt);
				tiempos[i] = System.nanoTime()*Math.pow(10, -9);
				pt.backtracking();
				tiempos[i] = System.nanoTime()*Math.pow(10, -9) - tiempos[i];
			}
			System.out.println("[");
			for (int i = 0; i < tiempos.length; i++) {
				System.out.print(tiempos[i]+" ,");
			}
			System.out.println("]");
			System.out.println();
			System.out.printf("%f s ",media(tiempos));
			numPropuestas += 1;
			posibilidadDoe /= 2;
			}

		}
		

	public static double media (double[] array) {
		double media = 0;
		for (int i = 0; i < array.length; i++) {
			media += array[i];
		}
		media /= array.length;
		return media;


	}

}
