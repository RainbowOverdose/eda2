package org.eda2.backtracking;

public class Acciones {
	private int numAccion;
	private int grupo;
	private int mesComienzo;
	private int mesFinal;
	private Requisitos recur;

	public Acciones(int num, int group, int mesIni, int mesFin, Requisitos req){
		this.numAccion = num;
		this.grupo = group;
		this.mesComienzo = mesIni;
		this.mesFinal = mesFin;
		this.recur = req;
	}

	public int getNumAccion() {
		return numAccion;
	}

	public void setNumAccion(int numAccion) {
		this.numAccion = numAccion;
	}

	public int getGrupo() {
		return grupo;
	}

	public int getMesComienzo() {
		return mesComienzo;
	}

	public void setMesComienzo(int mesComienzo) {
		this.mesComienzo = mesComienzo;
	}

	public int getMesFinal() {
		return mesFinal;
	}

	public void setMesFinal(int mesFinal) {
		this.mesFinal = mesFinal;
	}

	public Requisitos getRecur() {
		return recur;
	}
	public String toString(){
		return this.numAccion+" "+this.grupo+" "+this.getMesComienzo()+" "+this.getMesFinal()+" "+this.recur;
	}
}
