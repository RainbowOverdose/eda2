package org.eda2.dinamica;

import java.io.File;
import java.util.ArrayList;
import java.util.TreeMap;

public class PlanningOffice {

	public static void main(String[] args) {
		String archivoCiudad = System.getProperty("user.dir") + File.separator +
			    "src" + File.separator +
			    "org" + File.separator +
			    "eda2" + File.separator +
			    "divideyvenceras" + File.separator +
			    "ciudad16x16.txt";
		int[] array = {148, 34, 34, 48, 34 , 4, 17, 2, 11, 4, 17, 2, 11, 16, 17, 2, 11, 4, 17, 2, 11};
		DistrictPlanning dp = new DistrictPlanning(array);
		ArrayList<Integer> listaAbsentismo = dp.districtPlanningGreedy();
		for (int i = 0; i < listaAbsentismo.size(); i++) {

			System.out.print(listaAbsentismo.get(i).toString());
			System.out.println(" : " +array[listaAbsentismo.get(i)]);
		}

		System.out.println("");
		System.out.println("**********A partir de una matriz hecha a mano: **********");
		System.out.println("");

		int[][] matriz = {{4, 17, 2, 11, 4, 17, 2, 11},
							{4, 17, 2, 11, 4, 17, 2, 11},
							{4, 17, 2, 11, 4, 17, 2, 11},
							{4, 17, 2, 11, 4, 17, 2, 11},
							{4, 17, 2, 11, 4, 1052, 2, 11},
							{4, 17, 2, 11, 4, 17, 2, 11},
							{4, 28, 2, 11, 4, 17, 2, 11},
							{4, 17, 2, 11, 4, 17, 2, 11}};
		DistrictPlanning dp2 = new DistrictPlanning(matriz);
		ArrayList<Integer> listaAbsentismo2 = dp2.districtPlanningGreedy();
		int[] array2 = dp2.getDistritos();
		for (int i = 0; i < listaAbsentismo2.size(); i++) {

			System.out.print(listaAbsentismo2.get(i).toString());
			System.out.println(" : " +array2[listaAbsentismo2.get(i)]);

		}

		System.out.println();
		System.out.println("********* Valores pasados a Manhattan: **********");
		System.out.println();

		TreeMap<Integer, Manzana> mapaManhattanAbsentismo = dp2.toManhattan(listaAbsentismo2);
		for (int i : mapaManhattanAbsentismo.keySet()) {
			System.out.println(i + ": "+array2[i] + " --> " + mapaManhattanAbsentismo.get(i));

		}

		int[][] matriz2 = new int[1024][1024];

		for (int i = 0; i < matriz2.length; i++) {
			for (int j = 0; j < matriz2[i].length; j++) {
				matriz2[i][j] = (int)(Math.random()*31);
			}
		}
		DistrictPlanning dp3 = new DistrictPlanning(matriz2);
		dp3.districtPlanningGreedy();

		//DistrictPlanning dp4 = new DistrictPlanning(archivoCiudad);
		//dp4.districtPlanningGreedy();

		//B//
		int[][] matriz3 = {{5,20,1,0,5,3,2,1},
						   {12,9,2,7,4,7,2,4},
						   {1,0,8,7,5,3,2,3},
						   {2,7,10,1,2,1,7,6},
						   {6,7,4,1,83,13,7,8},
						   {12,1,3,5,6,3,1,8},
						   {50,4,3,2,1,7,6,8},
						   {67,3,4,2,3,6,8,9}};

		DistrictPlanning dp5 = new DistrictPlanning(matriz3);
		dp5.districtPlanningGreedy();

		ArrayList<GrupoTrabajo> gt = dp5.workGroupsGreedy();
		System.out.println("FIN");

		/***********************DINAMICA*****************************/
		ArrayList<Double> peso = new ArrayList<>();
		peso.add(2d);
		peso.add(3d);
		peso.add(4d);
		peso.add(6d);


		System.out.println(peso);

		ArrayList<Double> beneficio = new ArrayList<Double>();
		beneficio.add(14d);
		beneficio.add(22d);
		beneficio.add(35d);
		beneficio.add(40d);

		System.out.println(beneficio);

		ProjectPlanning pepe = new ProjectPlanning(peso, beneficio, 8d);
		ArrayList<Integer> sol = pepe.ProyectosMochilaDinamica();
		System.out.println(sol);

		/***********************DINAMICA2 *****************************/
		ArrayList<Propuesta> prp = new ArrayList<>();
		prp.add(new Propuesta(1, 1, 47, 1, 5));
		prp.add(new Propuesta(3, 2, 77, 1, 4));
		prp.add(new Propuesta(2, 3, 26, 1, 10));
		prp.add(new Propuesta(1, 4, 37, 2, 7));
		prp.add(new Propuesta(4, 5, 37, 1, 8));
		prp.add(new Propuesta(2, 6, 37, 3, 6));
		prp.add(new Propuesta(3, 7, 87, 1, 7));
		prp.add(new Propuesta(1, 8, 57, 3, 2));
		prp.add(new Propuesta(2, 9, 107, 2, 5));
		prp.add(new Propuesta(4, 10, 37, 3, 7));
		prp.add(new Propuesta(3, 11, 27, 1, 8));
		prp.add(new Propuesta(1, 12, 36, 3, 4));
		prp.add(new Propuesta(2, 13, 107, 2, 8));
		prp.add(new Propuesta(3, 14, 27, 1, 7));
		prp.add(new Propuesta(4, 15, 27, 2, 8));

		ProjectPlanning pepe2 = new ProjectPlanning(gt,prp,200d,true);
		ArrayList<Integer> sol2 = pepe2.ProyectosMochilaDinamica();
		System.out.println("Soluci�n Final: "+sol2);
	}


}
