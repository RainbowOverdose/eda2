package org.eda2.dinamica;

import java.util.ArrayList;

public class ProjectPlanning {

	private int capacidad;
	private ArrayList<Integer> pesos;
	private ArrayList<Double> beneficios;
	private ArrayList<Integer> Sol;
	int[][] mochila;
	double mcd, presupuesto;

	public ProjectPlanning(ArrayList<Double> pesos, ArrayList<Double> beneficios, double presupuesto) {
		//constructor basico de pruebas
		this.presupuesto = presupuesto;
		mcd = maximoComunDivisor(pesos);
		if(mcd>0)
			this.capacidad = (int)(presupuesto/mcd);
		else
			this.capacidad = 0;
		this.pesos = toPesosInt(pesos);
		this.beneficios = beneficios;
		Sol = new ArrayList<Integer>();
		mochila = new int[pesos.size() + 1][capacidad + 1];
	}

	public ProjectPlanning(ArrayList<GrupoTrabajo> gruposTrabajo, ArrayList<Propuesta> propuestas, double presupuesto, boolean hola) {
		this.presupuesto = presupuesto;
		mcd = maximoComunDivisorP(propuestas);
		if(mcd>0)
			this.capacidad = (int)(presupuesto/mcd);
		else
			this.capacidad = 0;
		this.pesos = new ArrayList<>();
		this.beneficios = new ArrayList<>();
		//Pretratamiento para calcular los pesos de cada objeto y su correspondiente beneficio
		pret(propuestas, gruposTrabajo);
		Sol = new ArrayList<Integer>();
		mochila = new int[pesos.size() + 1][capacidad + 1];
		System.out.println("Pesos: "+this.pesos.toString());
		System.out.println("Beneficios: "+this.beneficios.toString());
	}
	//M�ximo com�n divisor seg�n un array donde ya tengamos los costes
	private double maximoComunDivisor(ArrayList<Double> costes) {
		double tmp, mult;
		int decimal=0;
		if(costes.size()>1){
			for(int i = 0;i<costes.size();i++){
				if((costes.get(i)+"").split("\\.")[1].length()>decimal)
					decimal=(costes.get(i)+"").split("\\.")[1].length();
			}
			mult = Math.pow(10, decimal);
			tmp = mcd((int)(costes.get(0)*mult),(int)(costes.get(1)*mult))/mult;
			for(int i=2;i<costes.size();i++){
				tmp = mcd((int)(tmp*mult),(int)(costes.get(i)*mult))/mult;
			}
			return tmp;
		}
		else if(costes.size()==1){
			for(int i = 0;i<costes.size();i++){
				if((costes.get(i)+"").split("\\.")[1].length()>decimal)
					decimal=(costes.get(i)+"").split("\\.")[1].length();
			}
			mult = Math.pow(10, decimal);
			tmp = mcd((int)(costes.get(0)*mult),(int)(presupuesto*mult))/mult;
			return tmp;
		}
		else
			return 0;
	}
	//M�ximo com�n multiplo de los costes de todas las propuestas
	private double maximoComunDivisorP(ArrayList<Propuesta> prop) {
		double tmp, mult;
		int decimal=0;
		if(prop.size()>1){
			for(int i = 0;i<prop.size();i++){
				if((prop.get(i).getCostePropuesta()+"").split("\\.")[1].length()>decimal)
					decimal=(prop.get(i).getCostePropuesta()+"").split("\\.")[1].length();
			}
			mult = Math.pow(10, decimal);
			tmp = mcd((int)(prop.get(0).getCostePropuesta()*mult),(int)(prop.get(1).getCostePropuesta()*mult))/mult;
			for(int i=2;i<prop.size();i++){
				tmp = mcd((int)(tmp*mult),(int)(prop.get(i).getCostePropuesta()*mult))/mult;
			}
			return tmp;
		}
		else if(prop.size()==1){
			for(int i = 0;i<prop.size();i++){
				if((prop.get(i).getCostePropuesta()+"").split("\\.")[1].length()>decimal)
					decimal=(prop.get(i).getCostePropuesta()+"").split("\\.")[1].length();
			}
			mult = Math.pow(10, decimal);
			tmp = mcd((int)(prop.get(0).getCostePropuesta()*mult),(int)(presupuesto*mult))/mult;
			return tmp;
		}
		else
			return 0;
	}
	//Algoritmo de Euclides
	private int mcd(int a, int b){
		int r;
		while(b!=0){
			r = a%b;
			a = b;
			b = r;
		}
		return a;
	}

	public ArrayList<Integer> ProyectosMochilaDinamica() {
		mochilaDinamica();
		recuperarSolucionDinamica();
		System.out.println("Soluci�n Antes de Sobrepasarla: "+Sol);
		int pesoActual=0;
		if(Sol.size()!=0){
			int min=Integer.MAX_VALUE;
			for(int i=0;i<Sol.size();i++){
				pesoActual+=pesos.get(Sol.get(i)-1);
				if(pesos.get(Sol.get(i)-1)<min)
					min=pesos.get(Sol.get(i)-1);
			}
			if(this.capacidad-pesoActual>0){
			ArrayList<Integer> pesosMenores=getMinPesos(min);
				if(pesosMenores.size()>0){
					int maxBen=-1;
					for(int i=0;i<pesosMenores.size();i++){
						if(beneficios.get(pesosMenores.get(i))>maxBen)
							maxBen=pesosMenores.get(i);
					}
					if(maxBen!=-1)
						Sol.add(0,maxBen+1);
				}
			}
		}
		System.out.println("Soluci�n Final: "+Sol.toString());
		return Sol;
	}
	//Castear un array de n�meros reales a enteros
	public ArrayList<Integer> toPesosInt(ArrayList<Double> pes){
		ArrayList<Integer> pesInt = new ArrayList<>();
		for (Double d : pes) {
			pesInt.add((int) (d/mcd));
		}
		return pesInt;
	}
	//Obtiene un ArrayList con todos los pesos menores que @param="min" y que no contenga la soluci�n.
	public ArrayList<Integer> getMinPesos(int min){

		ArrayList<Integer> pesosMenores = new ArrayList<>();
		for(int i=0;i<this.pesos.size();i++){
			if(pesos.get(i)<min && !Sol.contains(i+1))
				pesosMenores.add(i);
		}
		return pesosMenores;
	}
	//Pretratamiento del ArrayList propuestas y grupos de trabajos y rellenar pesos y beneficios
	public void pret(ArrayList<Propuesta> propuestas, ArrayList<GrupoTrabajo> gruposTrabajo){
		int st=0;
		for(GrupoTrabajo g: gruposTrabajo){
			st+=g.getSuperficieCubierta();
		}
		for (Propuesta p : propuestas) {
			GrupoTrabajo group = gruposTrabajo.get(p.getGrupoTrabajo()-1);
			/*System.out.println("Coste Propuesta:"+p.getCostePropuesta());
			System.out.println("Grupo de Trabajo:"+p.getGrupoTrabajo());
			System.out.println("Indicador de Impacto:"+p.getIndicadorImpacto());
			System.out.println("N�mero de propuesta:"+p.getNumeroPropuesta());
			System.out.println("Orden de prioridad:"+p.getOrdenPrioridad());*/
			this.pesos.add((int)(p.getCostePropuesta()/mcd));
			double sup = (double)group.getSuperficieCubierta()/(double)st;
			double sup2 = sup*(1/(double)p.getOrdenPrioridad());
			double sup3 = (double)p.getIndicadorImpacto()*(double)group.getAbsentismoTotal()*sup2;
			//System.out.println("sup "+group.getSuperficieCubierta()+"st "+st);
			/*System.out.println("1 "+sup);
			System.out.println("2 "+sup2);
			System.out.println("3 "+sup3);*/
			this.beneficios.add(sup3);
		}
	}

	private void mochilaDinamica() {
		// rellenamos la primera fila y la primera columna de ceros
		for (int i = 0; i < pesos.size(); i++) {
			mochila[i][0] = 0;
		}
		for (int i = 0; i < capacidad; i++) {
			mochila[0][i] = 0;
		}

		for (int i = 1; i <= pesos.size(); i++) {
			for (int j = 1; j <= capacidad; j++) {
				if (j < pesos.get(i - 1)) {
					mochila[i][j] = mochila[i - 1][j];
				} else {
					if (mochila[i - 1][j] > mochila[i - 1][(int)(j - pesos.get(i - 1))] + beneficios.get(i - 1)) {
						mochila[i][j] = mochila[i - 1][j];
					} else {
						mochila[i][j] = (int) (mochila[i - 1][(int)(j - pesos.get(i - 1))] + beneficios.get(i - 1));
					}
				}
			}
		}
	}

	private void recuperarSolucionDinamica() {
		recuperarSolucion(pesos.size(), capacidad);
	}

	//c es la capacidad temporal que disminuye
	private void recuperarSolucion(int j, int c) {
		if (j > 0) {
			if (c < pesos.get(j-1)) {
				recuperarSolucion(j-1, c); //si el elemento no cabe, pasamos a uno de menor tama�o
			}else {
				if (mochila[j][c] > mochila[j-1][c]) { //si el objeto es parte de la soluci�n se quita
																				//esa parte a la capacidad y se pasa al siguiente objeto
					recuperarSolucion(j-1,(int)(c-pesos.get(j-1)));
					Sol.add(j);          										//ademas, se a�ade el objeto a la soluc�n
				}else {
					recuperarSolucion(j-1, c);									//si no es parte de la soluci�n, continua al suiguiente objto de pero menor
				}
			}
		}
	}
}
