package org.eda2.dinamica;
//ANEXO 4
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

public class DistrictPlanning {

	private int[] distritos;
	private ArrayList<Integer> listaAbsentismo;
	private int numeroManzanas;
	private TreeMap<Integer, Manzana> fractalToManhattan;

	public DistrictPlanning(int[] distritos) {
		 this.distritos = distritos;
		 listaAbsentismo = new ArrayList<Integer>();
		 numeroManzanas = potenciaDeCuatroContenida(distritos.length);
		 fractalToManhattan = new TreeMap<Integer, Manzana>();
	}

	public DistrictPlanning(int[][] manzanas) {
		numeroManzanas = manzanas.length * manzanas[0].length;
		int unaDimension = 0;
		int resto = manzanas.length * manzanas[0].length;
		while(resto > 0) {
			unaDimension += resto;
			resto = resto / 4;
		}
		distritos = new int[unaDimension];
		procesamientoDatos(manzanas);
		listaAbsentismo = new ArrayList<Integer>();
		fractalToManhattan = new TreeMap<Integer, Manzana>();
	}

	public DistrictPlanning(String archivo){
		fractalToManhattan = new TreeMap<Integer, Manzana>();
		listaAbsentismo = new ArrayList<Integer>();
		ArrayList<Integer> lista = new ArrayList<Integer>();
		int numero = 0;
		this.numeroManzanas = potenciaDeCuatroContenida(lista.size());
		int numeroNoManzanas = lista.size() - numeroManzanas;
		int sumaHijos = 0;
		try{
		File file = new File(archivo);
		BufferedReader br = new BufferedReader(new FileReader(file));
		String linea = "";
		while(br.ready()){
			linea = br.readLine();
			numero = Integer.parseInt(linea);
			if(numero%1==0)
				lista.add(numero);
		}
		br.close();

		if (sumaPotenciasMenores(4, (int)logOfBase(4, numeroManzanas))!=lista.size()) {
			throw new RuntimeException("Hubo una perdida de datos");
		}

		for (int i = 0; i < numeroNoManzanas; i++) {
			sumaHijos = lista.get(i*4+1) + lista.get(i*4+2) + lista.get(i*4+3) + lista.get(i*4+4);
			if(sumaHijos != lista.get(i))
				throw new RuntimeException("Datos corruptos");
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
		finally {
			distritos = new int[lista.size()];
			for (int i = 0; i < distritos.length; i++) {
				distritos[i] = lista.get(i);
			}
		}
	}

	// Procesamiento de datos

	//llamado desde el constructor de planificaci�n del distrinto para rellenar la estructura de datos
	public void procesamientoDatos(int[][] matrizManzanas) {
		procesamientoManzanasDyV(matrizManzanas, matrizManzanas.length-1, 0, 0, matrizManzanas[0].length-1, 0);

	}

	private void procesamientoManzanasDyV(int[][] matrizManzanas, int ComienzoX, int ComienzoY, int FinX, int FinY, int current) {
		if (current >= distritos.length)return;

		int mediaX = (ComienzoX + FinX)/2;
		int mediaY = (ComienzoY + FinY)/2;

		int aux = 0;
		for (int i = ComienzoX; i >= FinX; i--) {
			for (int j = ComienzoY; j <= FinY; j++) {
				aux += matrizManzanas[i][j];
			}
		}
		distritos[current] = aux;

		if(4*current+2 > distritos.length) return;
		procesamientoManzanasDyV(matrizManzanas, ComienzoX, ComienzoY, mediaX+1, mediaY, 4*current+1);
		procesamientoManzanasDyV(matrizManzanas, mediaX, ComienzoY, FinX, mediaY, 4*current+2);
		procesamientoManzanasDyV( matrizManzanas, mediaX, mediaY+1, FinX, FinY, 4*current+3);
		procesamientoManzanasDyV(matrizManzanas, ComienzoX, mediaY+1, mediaX+1, FinY, 4*current+4);
	}

	public ArrayList<Integer> getListaAbsentismo() {
		return listaAbsentismo;
	}

	public int[] getDistritos() {
		return distritos;
	}

	// Divide y venceras

	// Devolvera una lista con las manzanas con absentismo
	// la matriz de manzanas es temporal, en cuanto este preparado el procesamiento de datos, se usara.

	public ArrayList<Integer> districtPlanningGreedy() {
		//int current = 0;
		int niveles = (int)logOfBase(4, numeroManzanas);
		ArrayList<ArrayList<Integer>> lista = new ArrayList<ArrayList<Integer>>();
		lista.add(new ArrayList<Integer>());
		lista.get(0).add(0);
		for (int i = 0; i < niveles; i++) {
			lista.add(new ArrayList<Integer>());
			for (Integer espacioFractal : lista.get(i)) {
				if (distritos[espacioFractal*4+1] > 15) lista.get(i+1).add(espacioFractal*4+1);
				if (distritos[espacioFractal*4+2] > 15) lista.get(i+1).add(espacioFractal*4+2);
				if (distritos[espacioFractal*4+3] > 15) lista.get(i+1).add(espacioFractal*4+3);
				if (distritos[espacioFractal*4+4] > 15) lista.get(i+1).add(espacioFractal*4+4);
			}
		}

		listaAbsentismo.addAll(lista.get(lista.size()-1));

		return listaAbsentismo;
	}

	private TreeMap<Integer, Manzana> workGroupsrAux() {
		int manzanas = distritos.length - numeroManzanas; //guardamos la posicion del array distritos donde empiezan los distritos
		ArrayList<Integer> puntosPorAsignar = new ArrayList<Integer>();
		for (int i = manzanas; i < distritos.length; i++) {
			if (!listaAbsentismo.contains(i)) {
				if (distritos[i]!=0) {
					puntosPorAsignar.add(i);
				}
			}
		}
		return toManhattan(puntosPorAsignar);
	}

	public ArrayList<GrupoTrabajo> workGroupsGreedy() {
		TreeMap<Integer, Manzana> porAsignar = (TreeMap<Integer, Manzana>)workGroupsrAux().clone();
		TreeMap<Integer, Manzana> criticos = toManhattan(listaAbsentismo);//supone que la lista de puntos criticos se ha sacado antes
		int criticoMasCercano = -1;
		int distanciaCritico = Integer.MAX_VALUE;
		int distanciaTmp = 0;

		ArrayList<Manzana> manzanasCriticas = new ArrayList<Manzana>();
		manzanasCriticas.addAll(criticos.values());
		ComparadorCritico cmp = new ComparadorCritico();
		manzanasCriticas.sort(cmp);
		//aqui creamos todos los grupos de trabajo numerados de 0 a n ordenados por su absentismo
		ArrayList<GrupoTrabajo> grupos = new ArrayList<GrupoTrabajo>();
		if(criticos.isEmpty()) return grupos;
		for (int i = 0; i < manzanasCriticas.size(); i++) {
			grupos.add(new GrupoTrabajo(i, manzanasCriticas.get(i)));
		}

		for (int i : porAsignar.keySet()) {
			criticoMasCercano = -1;
			distanciaCritico = Integer.MAX_VALUE;
			distanciaTmp = 0;
			//en vez de trabajar sobre criticos, trabajar sobre la lista de grupos de trabajo
			for (int j = 0; j < grupos.size(); j++) {
				distanciaTmp = porAsignar.get(i).distancia(grupos.get(j).getManzanaCritica());
				if (distanciaTmp<distanciaCritico) {
					distanciaCritico = distanciaTmp;
					criticoMasCercano = j;
				}
			}
			grupos.get(criticoMasCercano).addManzana(porAsignar.get(i));
		}
		//al terminar esto devuelve una estructura de datos con los grupos de trabajo
		return grupos;
	}

	private int potenciaDeCuatroContenida(int numero) {
		int aux = 1;
		while(aux < numero) {
			aux*=4;
		}
		return aux/4;
	}

	private int sumaPotenciasMenores(int x, int e) {
		int aux = 0;
		for (int i = 0; i <= e; i++) {
			aux += Math.pow(x, e);
		}
		return aux;
	}

	public double logOfBase(int base, int num) {
	    return Math.log(num) / Math.log(base);
	}

	// Post procesamiento - Transformaci�n de posiciones

	// Duevuelve una tabla que contiene la traducción de distritos a formato de manhattan
	public TreeMap<Integer, Manzana> toManhattan(ArrayList<Integer> lista) {
		fractalToManhattan.clear();
		int aux = (int) Math.sqrt(potenciaDeCuatroContenida(distritos.length));

		toManhattan(lista, aux-1, 0, 0, aux-1, 0);

		return fractalToManhattan;
	}

	//metodo fractal
	private void toManhattan(ArrayList<Integer> absentismo, int inicioX, int inicioY, int finX, int finY, int current) {
		if (current >= distritos.length)return;
		int mediaX = (inicioX + finX)/2;
		int mediaY = (inicioY + finY)/2;
		Manzana manzana = new Manzana(inicioX, inicioY, distritos[current]);
		if(absentismo.contains(current)) {
			fractalToManhattan.put(current, manzana);
		}

		if(4*current+1 > distritos.length) return;
		toManhattan(absentismo, inicioX, inicioY, mediaX+1, mediaY, 4*current+1);
		toManhattan(absentismo, mediaX, inicioY, finX, mediaY, 4*current+2);
		toManhattan(absentismo, mediaX, mediaY+1, finX, finY, 4*current+3);
		toManhattan(absentismo, inicioX, mediaY+1, mediaX+1, finY, 4*current+4);
	}
}
