package org.eda2.dinamica;

import java.util.ArrayList;

public class MedidasRendimiento {

	public static void main(String[] args) {
		//aqui se medira el tiempo de ejecucion para distintos tama�os n
		// y distinta complejidad
		int citySize = 2;
		double[] tiempos = new double[13];
		int count = 0;
		int aux = 0;

		//mejor caso que el mcd sea muy alto, entonces reduce la tabla mucho mas (tabla de n * m)este sera el m
		//el n se reduce si cogemos menos objetos con mas peso.
		System.out.println("MEJOR CASO");
		for (int k = 0; k < 10; k++) {
			int[][] matriz = new int[citySize][citySize];

			for (int l = 0; l < 10; l++) {
				count = 0;
				aux = citySize/2;
				for (int i = 0; i < matriz.length; i++) {
					for (int j = 0; j < matriz[i].length; j++) {
						matriz[i][j] = (int)(Math.random()*17);
					}
				}
				matriz[0][0] = 25;//nos aseguramos que exista al menos una manzana que supere el absentismo


				DistrictPlanning dp = new DistrictPlanning(matriz);
				dp.districtPlanningGreedy();
				ArrayList<GrupoTrabajo> gt = dp.workGroupsGreedy();
				ArrayList<Propuesta> p = new ArrayList<Propuesta>();
				int group = 0;
				int orden = 0;
				for(int i = 0; i < gt.size()*3; i++){
					orden= 1;
					group = (int)(Math.random()*gt.size()+1);
					for(int j = 0; j < p.size(); j++ ){
						if (p.get(j).getGrupoTrabajo() == group ) {
							orden++;
						}
					}

					p.add(new Propuesta(group, i, (int)(Math.random()*7+1)*5, orden, (int)Math.random()*4+1));
				}
				ProjectPlanning pp = new ProjectPlanning(gt, p, gt.size()*20, true);
				tiempos[l] = System.nanoTime()*Math.pow(10, -9);
				pp.ProyectosMochilaDinamica();
				tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
			}
			System.out.printf("%f s ",media(tiempos));
			citySize *= 2;
		}
		/*
		System.out.println("PEOR CASO");
		//El peor caso es que el mcd sea muy bajo, entonces aumenta la tabla mucho mas (tabla de n * m)este sera el m
		//el n aumenta si cogemos mayor numero de objetos con menor peso.
		for (int k = 0; k < 10; k++) {
			int[][] matriz = new int[citySize][citySize];

			for (int l = 0; l < 10; l++) {
				count = 0;
				aux = citySize/2;
				for (int i = 0; i < matriz.length; i++) {
					for (int j = 0; j < matriz[i].length; j++) {
						matriz[i][j] = (int)(Math.random()*30);
					}
				}
				matriz[0][0] = 25;//nos aseguramos que exista al menos una manzana que supere el absentismo


				DistrictPlanning dp = new DistrictPlanning(matriz);
				dp.districtPlanningGreedy();
				ArrayList<GrupoTrabajo> gt = dp.workGroupsGreedy();
				ArrayList<Propuesta> p = new ArrayList<Propuesta>();
				int group = 0;
				int orden = 0;
				for( int i = 0; i < gt.size()*3; i++){
					orden= 1;
					group = (int)(Math.random()*gt.size()+1);
					for(int j = 0; j < p.size(); j++ ){
						if (p.get(j).getGrupoTrabajo() == group ) {
							orden++;
						}
					}

					p.add(new Propuesta(group, i, (int)(Math.random()*35+1), orden, (int)Math.random()*4+1));
				}
				ProjectPlanning pp = new ProjectPlanning(gt, p, gt.size()*20, true);
				tiempos[l] = System.nanoTime()*Math.pow(10, -9);
				pp.ProyectosMochilaDinamica();
				tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
			}
			System.out.printf("%f s ",media(tiempos));
			citySize *= 2;

		}*/
	}

	public static double media (double[] array) {
		double media = 0;
		for (int i = 0; i < array.length; i++) {
			media += array[i];
		}
		media /= array.length;
		return media;


	}

}
