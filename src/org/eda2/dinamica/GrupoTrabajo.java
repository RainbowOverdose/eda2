package org.eda2.dinamica;

import java.util.ArrayList;
//ANEXO 1
/**
 * Es la clase que debe devolver el aparatado B y que luego
 * recibiremos como entrada de la practica de Programacion Dinamica.
 *
 */
public class GrupoTrabajo{

	private int grupoTrabajo;
	private Manzana manzanaCritica;
	private int superficieCubierta;
	private int absentismoTotal;
	ArrayList<Manzana> grupo;

	/**
	 * Constructor de la clase,
	 * @param grupoTrabajo define el numero que corresponde a este grupo de trabajo.
	 * @param m es la manzana critica.
	 * La superficie total cubierta se pone a 1 al crear el GT solo con la manzana critica
	 * y adem�s inicializamos el ArrayList de manzanas pertenecientes al Grupo.
	 */
	public GrupoTrabajo(int grupoTrabajo, Manzana m) {
		this.grupoTrabajo = grupoTrabajo;
		manzanaCritica = m;
		superficieCubierta = 1;
		absentismoTotal = m.getAbsentismo();
		grupo = new ArrayList<Manzana>();
	}

	public Manzana getManzanaCritica() {
		return manzanaCritica;
	}

	public int getGrupoTrabajo() {
		return this.grupoTrabajo;
	}

	public int getSuperficieCubierta() {
		return superficieCubierta;
	}

	public int getAbsentismoTotal() {
		return absentismoTotal;
	}

	/**
	 * A�ade una manzana a nuestra coleccion de manzanas del grupo de trabajo,
	 * ademas aumentas la superficie de este y actualiza el valor del
	 * absentismo total del grupo de trabajo.
	 * @param m es la manzana que se a�ade al GT
	 */
	public void addManzana(Manzana m) {
		superficieCubierta++;
		absentismoTotal += m.getAbsentismo();
		grupo.add(m);
	}



}
