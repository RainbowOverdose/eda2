package org.eda2.dinamica;

import java.util.Comparator;
//ANEXO 2
public class ComparadorCritico implements Comparator<Manzana>{

	/**
	 * Compara dos manzanas segun su absentismo. Ordena de mayor a menor.
	 */
	public int compare(Manzana m1, Manzana m2) {
		if (m1.getAbsentismo() > m2.getAbsentismo()) {
			return -1;
		}else if (m2.getAbsentismo() > m1.getAbsentismo()) {
			return 1;
		}
		return 0;
	}
}
