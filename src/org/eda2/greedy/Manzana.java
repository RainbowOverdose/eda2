package org.eda2.greedy;

public class Manzana {
	private int x;
	private int y;

	private int absentismo;

	/**
	 * Es el constructor d ela clase y se diferencia en el usado en DyV por el atributo absentismo.
	 * @param x
	 * @param y
	 * @param absentismo
	 */
	public Manzana(int x, int y, int absentismo) {
		this.x = x;
		this.y = y;
		this.absentismo = absentismo;
	}

	public int getX() {
		return x;
	}

	public int getAbsentismo() {
		return absentismo;
	}

	public int getY() {
		return y;
	}

	/**
	 * A�adimos el metodo que nos permite calcular la distancia entre manzanas.
	 * @param m la manzana hasta que se desea calcular la distancia
	 * @return un entero que es la distancia de manhattan entre las dos manzanas
	 */
	public int distancia(Manzana m) {
		return Math.abs(m.getX()-this.getX())+Math.abs(m.getY()-this.getY());
	}

	public String toString() {
		return "["+x+", "+y+"]";
	}
}
