package org.eda2.greedy;

public class MedidasRendimiento {

	public static void main(String[] args) {
/*		//aqui se medira el tiempo de ejecucion para distintos tama�os n
		// y distinta complejidad
		int citySize = 2;
		double[] tiempos = new double[13];

		//mejor caso definido como absentismo de 0-3
		for (int k = 0; k < 10; k++) {
			int[][] matriz = new int[citySize][citySize];

			for (int l = 0; l < 10; l++) {
				for (int i = 0; i < matriz.length; i++) {
					for (int j = 0; j < matriz[i].length; j++) {
						matriz[i][j] = (int)(Math.random()*4);
					}
				}

				DistrictPlanning dp = new DistrictPlanning(matriz);
				tiempos[l] = System.nanoTime()*Math.pow(10, -9);
				dp.districtPlanningGreedy();
				tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
			}
			System.out.printf("%f s ",media(tiempos));
			citySize *= 2;
		}

		System.out.println();
		citySize = 2;

		//peor caso definido como absentismo de > 15 la mayoria
		for (int k = 0; k < 10; k++) {
			int[][] matriz = new int[citySize][citySize];

			for (int l = 0; l < 10; l++) {
				for (int i = 0; i < matriz.length; i++) {
					for (int j = 0; j < matriz[i].length; j++) {
						matriz[i][j] = (int)(Math.random()*30+14);
					}
				}

				DistrictPlanning dp = new DistrictPlanning(matriz);
				tiempos[l] = System.nanoTime()*Math.pow(10, -9);
				dp.districtPlanningGreedy();
				tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
			}
			System.out.printf("%f s ",media(tiempos));
			citySize *= 2;
		}*/
		//B//
		//aqui se medira el tiempo de ejecucion para distintos tama�os n
				// y distinta complejidad
				int citySize = 2;
				double[] tiempos = new double[15];
				System.out.println("TIEMPOS B");
				//mejor caso definido como absentismo de 0-3
				for (int k = 0; k < 9; k++) {
					int[][] matriz = new int[citySize][citySize];

					for (int l = 0; l < 1; l++) {
						for (int i = 0; i < matriz.length; i++) {
							for (int j = 0; j < matriz[i].length; j++) {
								matriz[i][j] = (int)(Math.random()*4);
							}
						}

						DistrictPlanning dp = new DistrictPlanning(matriz);
						dp.districtPlanningGreedy();
						tiempos[l] = System.nanoTime()*Math.pow(10, -9);
						dp.workGroupsGreedy();
						tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
					}
					System.out.printf("%f s ",media(tiempos));
					citySize *= 2;
				}

				System.out.println();
				citySize = 2;

				//peor caso definido como absentismo mitad menor de 15, mitad mayor de 15
				for (int k = 0; k < 9; k++) {
					int[][] matriz = new int[citySize][citySize];

					for (int l = 0; l < 1; l++) {
						for (int i = 0; i < matriz.length; i++) {
							for (int j = 0; j < matriz[i].length; j++) {
								matriz[i][j] = (int)(Math.random()*30);
							}
						}

						DistrictPlanning dp = new DistrictPlanning(matriz);
						dp.districtPlanningGreedy();
						tiempos[l] = System.nanoTime()*Math.pow(10, -9);
						dp.workGroupsGreedy();
						tiempos[l] = System.nanoTime()*Math.pow(10, -9) - tiempos[l];
					}
					System.out.printf("%f s ",media(tiempos));
					citySize *= 2;
				}
	}

	public static double media (double[] array) {
		double media = 0;
		for (int i = 0; i < array.length; i++) {
			media += array[i];
		}
		media /= array.length;
		return media;


	}

}
