#### University of Almeria 2015-2016
##### Data Structures and Algorithms II(second quarter of second year)

* first practice - Divide and rule
* second practice - Greedy
* third practice - Dynamic programming
* forth practice - Backtracking
* fifth practice - Branch and bound(not implemented in this repository)

## MANTAINICE FINISHED

#### Members:

- Jesús Alberto Rubio Herrera
- Jose Manuel Mateo Martín
- Nikita Panteleev